USE_PKGBUILD=1
include /usr/local/share/luggage/luggage.make
TITLE=EndNoteX602Update
REVERSE_DOMAIN=edu.umich
VERSION=1.0
PAYLOAD=get-ENX6-update pack-ENX6-update pack-script-postinstall
LATEST_ENX6_URL=`curl -s http://endnote.com/downloads/available-updates | grep -o "http.*X6.*\.zip" -m 1`

get-ENX6-update:
	@sudo curl -s -O ${LATEST_ENX6_URL}
	@sudo unzip -qq -o *.zip

APP_DIR=`find . -type dir -depth 2 -name "EndNote*app"`
	
pack-ENX6-update:
	@sudo mkdir ${WORK_D}/tmp
	@sudo ${CP} "${APP_DIR}/Contents/Resources/Patchfile.patch" ${WORK_D}/tmp
	@sudo ${CP} "${APP_DIR}/Contents/Resources/applyPatch" ${WORK_D}/tmp